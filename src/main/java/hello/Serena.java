package hello;

/**
 * Created by xialeizhou on 9/16/15.
 */
public class Serena {
    private final long id;
    private final String name;

    public Serena(long id, String name) {
        this.id = id;
        this.name = name;
    }
    public long getId() {
        return this.id;
    }
    public String  getName() {
        return this.name;
    }
}
