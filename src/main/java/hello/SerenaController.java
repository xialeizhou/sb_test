package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xialeizhou on 9/16/15.
 */
@RestController
public class SerenaController {
    public static final String template = "Hello, %s";

    @RequestMapping("/greeting")
    public Serena greeting(@RequestParam(value = "name", defaultValue = "world") String name) {
       return new Serena(21, String.format(template, name));
    }
}
